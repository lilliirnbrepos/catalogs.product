package com.freeingreturns.catalogs.product;

import java.util.Set;

import com.freeingreturns.catalogs.product.state.LineItem;
import com.freeingreturns.catalogs.product.state.Repository;
import com.freeingreturns.roe.product.RplyLineItemAdd;
import com.freeingreturns.roe.product.RplyLineItemQuery;
import com.freeingreturns.roe.product.RqstLineItemAdd;
import com.freeingreturns.roe.product.RqstLineItemQuery;
import com.freeingreturns.utils.FRLogger;
import com.freeingreturns.utils.FRUnhandleExcpetionCatcher;
import com.neeve.aep.AepEngine;
import com.neeve.aep.AepMessageSender;
import com.neeve.aep.IAepApplicationStateFactory;
import com.neeve.aep.annotations.EventHandler;
import com.neeve.aep.event.AepMessagingStartedEvent;
import com.neeve.cli.annotations.Command;
import com.neeve.cli.annotations.Configured;
import com.neeve.rog.log.RogLog;
import com.neeve.rog.log.RogLogCdcProcessor;
import com.neeve.server.app.annotations.AppHAPolicy;
import com.neeve.server.app.annotations.AppInjectionPoint;
import com.neeve.server.app.annotations.AppStat;
import com.neeve.server.app.annotations.AppStatContainersAccessor;
import com.neeve.server.app.annotations.AppStateFactoryAccessor;
import com.neeve.sma.MessageView;
import com.neeve.stats.IStats.Counter;
import com.neeve.stats.StatsFactory;
import com.neeve.trace.Tracer.Level;

@AppHAPolicy(value = AepEngine.HAPolicy.StateReplication)
public class ProductCatalogMain {
    @AppStat(name = "DBConnection")
    private boolean isConnected;

    private CDCPrdHandler handler;
    private RogLog log;
    private RogLogCdcProcessor processor;

    private AepMessageSender messageSender;
    private AepEngine aepEngine;

    @Configured(property = "service.product.channels.replyChannel", defaultValue = "ProductReplyChannel")
    private String rplyChannel;

    @AppStat
    private final Counter reqeuestedAdds = StatsFactory.createCounterStat("Request-add Count");

    @AppStat
    private final Counter itemCount = StatsFactory.createCounterStat("LineItem Count");





    public ProductCatalogMain() {
        Thread.setDefaultUncaughtExceptionHandler(FRUnhandleExcpetionCatcher.getInstance());
    }





    @AppStateFactoryAccessor
    final public IAepApplicationStateFactory getStateFactory() {
        return new IAepApplicationStateFactory() {
            @Override
            final public Repository createState(MessageView view) {
                return Repository.create();
            }
        };
    }





    @AppInjectionPoint
    final public void setMessageSender(AepMessageSender msgSender) {
        this.messageSender = msgSender;
    }





    @AppInjectionPoint
    public void injectAepEngine(AepEngine engine) {
        aepEngine = engine;
    }





    @EventHandler
    final public void onMessagingStarted(final AepMessagingStartedEvent event) {
        Thread t = new Thread(new Runnable() {
            public void run() {
                FRLogger.logAsIs(Level.INFO, "Engine activated, starting CDC");
                try {
                    startCdc();
                }
                catch (Exception e_) {
                    e_.printStackTrace();
                }
            }
        });
        t.start();

    }





    @AppStatContainersAccessor
    public void addAppStatContainerObjects(Set<Object> containers) {
        if (handler != null)
            containers.add(handler);
    }





    @Command(name = "Start CDC")
    public void startCdc() throws Exception {
        Boolean enabled = false;
        try {
            enabled = Boolean.valueOf(aepEngine.getStore().getPersister().getDescriptor().getProperty(RogLog.PROP_CDC_ENABLED));
            if (enabled) {
                log = (RogLog)aepEngine.getStore().getPersister();
                handler = new CDCPrdHandler();
                processor = log.createCdcProcessor(handler);
                processor.run();
                return;
            }
        }
        catch (Exception e) {
        }

        FRLogger.logAsIs(Level.WARNING, "CDC Storage disabled");
    }





    @Command(name = "Stop CDC")
    public void stopCdc() throws Exception {
        if (processor != null) {
            processor.close();
        }
        else {
            FRLogger.logAsIs(Level.WARNING, "Unable to register CDC for appstat, not avaialble");
        }
    }





    @EventHandler
    final public void onRqstLineItem(RqstLineItemAdd rqst, Repository repo) {
        RplyLineItemAdd rply = RplyLineItemAdd.create();
        rply.setMsgId(rqst.getMsgId());
        reqeuestedAdds.increment();

        LineItem li = repo.getProducts().get(rqst.getItemID());
        if (li == null) {
            // copy the data in
            li = LineItem.create();
            li.setDiscountFlag(rqst.getDiscountFlag());
            li.setDamageDiscountFlag(rqst.getDamageDiscountFlag());
            li.setActivationRequiredFlag(rqst.getActivationRequiredFlag());
            li.setRegistryEligibleFlag(rqst.getRegistryEligibleFlag());
            li.setItemID(rqst.getItemID());
            li.setItemProductID(rqst.getItemProductID());
            li.setPosDepartmentID(rqst.getPosDepartmentID());
            li.setItemDescription(rqst.getItemDescription());
            li.setAbbreviatedDescription(rqst.getAbbreviatedDescription());
            li.setKitSetCode(rqst.getKitSetCode());
            li.setMerchandiseHierarchyLevelCode(rqst.getMerchandiseHierarchyLevelCode());
            li.setTaxGroupID(rqst.getTaxGroupID());
            li.setMerchandiseHierarchyGroupID(rqst.getMerchandiseHierarchyGroupID());

            // some non-linetime stuff
            repo.getProducts().put(li.getItemID(), li);
            rply.setSucceeded(true);
            rply.setItemID(li.getItemID());
            rply.setRetaiStoreId(rqst.getRetaiStoreId());
            itemCount.increment();

            FRLogger.logfmt(Level.DEBUG, rqst.getMsgId(), rqst.getMessageChannel(),
                            "added line item, id: {0}", li.getItemID());
        }
        else {
            FRLogger.logfmt(Level.INFO, rqst.getMsgId(), rqst.getMessageChannel(),
                            "line item already existed, id: {0}", li.getItemID());
            rply.setSucceeded(false);
            rply.setErrorString("duplicate entry");
            rply.setItemID(rqst.getItemID());
            rply.setRetaiStoreId(rqst.getRetaiStoreId());
        }

        // done
        messageSender.sendMessage(rplyChannel, rply);
    }





    @EventHandler
    final public void onRqstLineItemQuery(RqstLineItemQuery msg, Repository repo) {
        RplyLineItemQuery rply = RplyLineItemQuery.create();
        rply.setMsgId(msg.getMsgId());
        rply.setItemID(msg.getItemID());

        //
        // find it
        //
        LineItem li = repo.getProducts().get(msg.getItemID());
        if (li == null) {
            FRLogger.logfmt(Level.DEBUG, msg.getMsgId(), msg.getMessageChannel(),
                            "product not found, id:{0}",
                            msg.getItemID());
            rply.setFound(false);
            messageSender.sendMessage(rplyChannel, rply);
            return;
        }

        rply.setFound(true);
        rply.setDiscountFlag(li.getDiscountFlag());
        rply.setDamageDiscountFlag(li.getDamageDiscountFlag());
        rply.setActivationRequiredFlag(li.getActivationRequiredFlag());
        rply.setRegistryEligibleFlag(li.getRegistryEligibleFlag());
        rply.setItemID(li.getItemID());
        rply.setItemProductID(li.getItemProductID());
        rply.setPosDepartmentID(li.getPosDepartmentID());
        rply.setItemDescription(li.getItemDescription());
        rply.setAbbreviatedDescription(li.getAbbreviatedDescription());
        rply.setKitSetCode(li.getKitSetCode());
        rply.setMerchandiseHierarchyLevelCode(li.getMerchandiseHierarchyLevelCode());
        rply.setTaxGroupID(li.getTaxGroupID());
        rply.setMerchandiseHierarchyGroupID(li.getMerchandiseHierarchyGroupID());
        rply.setDamageDiscountFlag(li.getDamageDiscountFlag());

        FRLogger.logfmt(Level.DEBUG, msg.getMsgId(), msg.getMessageChannel(),
                        "Query succeeded - product:{0}, short-description:{1}",
                        li.getItemID(),
                        li.getAbbreviatedDescription());
        messageSender.sendMessage(rplyChannel, rply);
    }

}
