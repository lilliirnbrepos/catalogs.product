package com.freeingreturns.catalogs.product.driver;

import static java.text.MessageFormat.format;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

import com.freeingreturns.roe.product.RplyLineItemAdd;
import com.freeingreturns.roe.product.RqstLineItemAdd;
import com.neeve.aep.AepEngine;
import com.neeve.aep.AepMessageSender;
import com.neeve.aep.annotations.EventHandler;
import com.neeve.cli.annotations.Command;
import com.neeve.cli.annotations.Configured;
import com.neeve.server.app.annotations.AppInjectionPoint;
import com.neeve.server.app.annotations.AppMain;
import com.neeve.server.app.annotations.AppStat;
import com.neeve.stats.IStats.Counter;
import com.neeve.stats.StatsFactory;
import com.neeve.trace.Tracer;
import com.neeve.trace.Tracer.Level;
import com.neeve.util.UtlGovernor;

import freeingreturns.catalogs.db.LocalIMDB;
import freeingreturns.catalogs.orm.DbLineItem;

/**
 * A test driver app for the Application.
 */
public class DbSeeder {
    private static final Tracer logger = Tracer.create("service.product", Level.DEBUG);
    private HashSet<Long> messagesSoFar;
    private CountDownLatch latch;

    private final AtomicReference<Thread> sendingThread = new AtomicReference<Thread>();
    private volatile AepMessageSender messageSender;
    private AepEngine engine;

    @Configured(property = "service.product.channels.requestCahnnel", defaultValue = "ProductRequestChannel")
    private String rqstChannel;

    @Configured(property = "service.product.tests.basic.sendRate")
    private int sendRate;

    @Configured(property = "sercice.product.seeder.autoStart", defaultValue = "false")
    private boolean autoStart;

    @AppStat
    private final Counter sentCount = StatsFactory.createCounterStat("BasicMessageFlow Count");

    /**
     * 
     */
    public DbSeeder() {
        messagesSoFar = new HashSet<>();
    }

    @AppInjectionPoint
    final public void setMessageSender(AepMessageSender messageSender) {
        this.messageSender = messageSender;
    }

    @AppInjectionPoint
    final public void setAepEngine(AepEngine engine) {
        this.engine = engine;
    }

    /**
     * Starts sending messages (in a background thread)
     */
    @Command(name = "send", displayName = "Send Messages", description = "Instructs the driver to send messages")
    public final void doSeed() throws Exception {
        engine.waitForMessagingToStart();

        // connect to db & load items
        LocalIMDB database = LocalIMDB.getInstance();
        database.connectToDb();
        database.loadLineItems();
        List<DbLineItem> list = database.getLineItems();
        int count = list.size();
        latch = new CountDownLatch(count);

        // start sending data
        final Thread thread = new Thread(new SenderThread(sendRate, list), "ProductSeeder Send Driver");
        startSending(thread);
    }

    /**
     * 
     */
    private void startSending(final Thread thread) throws InterruptedException {
        Thread oldThread = sendingThread.getAndSet(thread);
        if (oldThread != null) {
            oldThread.interrupt();
            oldThread.join();
        }
        thread.start();
    }

    /**
     * Stops the current sending thread (if active_ 
     */
    @Command(name = "stopSending", displayName = "Stop Sending", description = "Stops sending of messages.")
    final public void stop() throws Exception {
        Thread oldThread = sendingThread.getAndSet(null);
        if (oldThread != null) {
            oldThread.join();
        }
    }

    @EventHandler
    public void onRplyLineItemAdd(RplyLineItemAdd rply) {
        if (messagesSoFar.remove(rply.getMsgId())) {
            latch.countDown();
            if (logger.isEnabled(Level.DEBUG))
                logger.log(format("{0}::onRplyLineItemAdd:rply={1}, succeeded:{2}", getClass().getSimpleName(), rply.getMsgId(), rply.getSucceeded()), Level.DEBUG);
            return;
        }
        else {
            logger.log(format("ERROR - {0}::onRplyLineItemAdd:rply={1}, succeeded:{2}", getClass().getSimpleName(), rply.getMsgId(), rply.getSucceeded()), Level.WARNING);
        }
        return;
    }

    /**
     * Gets the number of messages sent by the sender. 
     * 
     * @return The number of messages sent by this sender.
     */
    @Command
    public long getSentCount() {
        return sentCount.getCount();
    }

    @AppMain
    public void run(String[] args) throws Exception {
        if (autoStart)
            doSeed();
    }

    /**
     * 
     */
    public final CountDownLatch getLatch() {
        if (latch == null) {
            try {
                Thread.sleep(100); // starting the thread takes time
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return latch;
    }

    /**
     * the person actually doing the work
     */
    private class SenderThread implements Runnable {
        private int rate;
        private List<DbLineItem> list;

        public SenderThread(int rate, List<DbLineItem> list) {
            this.list = list;
            this.rate = rate;
        }

        @Override
        public void run() {
            // make sure I'm good to start
            if (list == null || list.size() <= 0) {
                logger.log("No data to send!", Level.SEVERE);
                return;
            }

            DbLineItem dbli;
            UtlGovernor sendGoverner = new UtlGovernor(rate);
            Iterator<DbLineItem> itr = list.iterator();
            logger.log(format("About to send:{0}", list.size()), Level.INFO);

            while (itr.hasNext()) {
                if (sendingThread.get() != Thread.currentThread())
                    break; // someone swapped me out

                dbli = itr.next();
                RqstLineItemAdd msg = RqstLineItemAdd.create();
                msg.setMsgId(sentCount.getCount());
                msg.setDiscountFlag(dbli.getDiscountFlag().getValue());
                msg.setDamageDiscountFlag(dbli.getDamageDiscountFlag().getValue());
                msg.setActivationRequiredFlag(dbli.getDamageDiscountFlag().getValue());
                msg.setRegistryEligibleFlag(dbli.getRegistryEligibleFlag().getValue());
                msg.setItemID(dbli.getItemID().getValue());
                msg.setItemProductID(dbli.getItemProductID().getValue());
                msg.setPosDepartmentID(dbli.getPOSDepartmentID().getValue());
                msg.setItemDescription(dbli.getItemDescription().getValue());
                msg.setAbbreviatedDescription(dbli.getAbbreviatedDescription().getValue());
                msg.setKitSetCode(dbli.getKitSetCode().getValue());
                msg.setMerchandiseHierarchyLevelCode(dbli.getMerchandiseHierarchyLevelCode().getValue());
                msg.setTaxGroupID(dbli.getTaxGroupID().getValue());
                msg.setMerchandiseHierarchyGroupID(dbli.getMerchandiseHierarchyLevelCode().getValue());

                sentCount.increment();
                messagesSoFar.add(msg.getMsgId());
                messageSender.sendMessage(rqstChannel, msg);
                if (logger.isEnabled(Level.DIAGNOSE))
                    logger.log(format("sent RqstLineItmeAdd.msgid={0}", sentCount.getCount()), Level.DIAGNOSE);

                sendGoverner.blockToNext();
            }
            logger.log(format("Total number of msgs sent:{0}", sentCount.getCount()), Level.INFO);
        }
    }
}
