/****************************************************************************
 * FILE: CDCHandler.java
 * DSCRPT:
 ****************************************************************************/

package com.freeingreturns.catalogs.product;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import com.eaio.uuid.UUID;
import com.freeingreturns.catalogs.product.state.LineItem;
import com.freeingreturns.utils.FRLogger;
import com.neeve.rog.IRogChangeDataCaptureHandler;
import com.neeve.rog.IRogNode;
import com.neeve.trace.Tracer.Level;

import freeingreturns.catalogs.db.LocalIMDB;
import freeingreturns.catalogs.orm.DbLineItem;

public class CDCPrdHandler implements IRogChangeDataCaptureHandler {

    private LocalIMDB localDb;





    public CDCPrdHandler() {
        FRLogger.logAsIs(Level.SEVERE, "CDCCustHandler::CDCCustHandler()");
        localDb = LocalIMDB.getInstance();
        localDb.connectToDb();
    }





    @Override
    public boolean handleChange(UUID objectId, ChangeType changeType, List<IRogNode> entries) {

        if (changeType == ChangeType.Send || changeType == ChangeType.Noop) {
            return true;
        }

        boolean inserted = false;
        LineItem stitm;
        DbLineItem dbitm;
        for (IRogNode node : entries) {

            inserted = false;
            if (node instanceof LineItem) {
                stitm = (LineItem)node;
                FRLogger.logfmt(Level.INFO, "Received put, item:{0}", stitm.getItemID());

                dbitm = new DbLineItem();
                dbitm.getItemID().setValue(stitm.getItemID());
                dbitm.getItemProductID().setValue(stitm.getItemProductID());
                dbitm.getDiscountFlag().setValue(stitm.getDiscountFlag());
                dbitm.getDamageDiscountFlag().setValue(stitm.getDamageDiscountFlag());
                dbitm.getPOSDepartmentID().setValue(stitm.getPosDepartmentID());
                dbitm.getItemDescription().setValue(stitm.getItemDescription());
                dbitm.getAbbreviatedDescription().setValue(stitm.getAbbreviatedDescription());
                dbitm.getKitSetCode().setValue(stitm.getKitSetCode());
                dbitm.getMerchandiseHierarchyLevelCode().setValue(stitm.getMerchandiseHierarchyLevelCode());
                dbitm.getTaxGroupID().setValue(stitm.getTaxGroupID());
                dbitm.getActivationRequiredFlag().setValue(stitm.getActivationRequiredFlag());
                dbitm.getRegistryEligibleFlag().setValue(stitm.getRegistryEligibleFlag());
                dbitm.getMerchandiseHierarchyGroupID().setValue(stitm.getMerchandiseHierarchyGroupID());
                dbitm.getRecordCreationTimestamp().setValue(Timestamp.valueOf(LocalDateTime.now()));
                dbitm.getRecordLastModifiedTimestamp().setValue(Timestamp.valueOf(LocalDateTime.now()));

                if (changeType.equals(ChangeType.Put))
                    inserted = localDb.addLineItem(dbitm);
                if (changeType.equals(ChangeType.Update))
                    inserted = localDb.updateLineItem(dbitm);

                FRLogger.logfmt(Level.INFO, "[dbworker] - success:{0}, table:{1}, id:{2}",
                                Boolean.toString(inserted),
                                dbitm.getTableName(),
                                dbitm.getItemID().getValue());
            }
        }
        return true;

    }





    @Override
    public boolean onCheckpointComplete(long checkpointVersion) {
        //logger.log(format("onCheckpointComplete(checkpointVersion={0})", checkpointVersion), Level.INFO);
        return true;
    }





    @Override
    public void onCheckpointStart(long checkpointVersion) {
        //logger.log(format("onCheckpointStart(checkpointVersion={0})", checkpointVersion), Level.INFO);
    }





    @Override
    public void onLogComplete(int logNumber, LogCompletionReason reason, Throwable errorCause) {
        //logger.log(format("onLogComplete(logNumber={0})", logNumber), Level.INFO);
    }





    @Override
    public void onLogStart(int logNumber) {
        //logger.log(format("onLogStart(logNumber={0})", logNumber), Level.INFO);
    }





    @Override
    public void onWait() {}

}
