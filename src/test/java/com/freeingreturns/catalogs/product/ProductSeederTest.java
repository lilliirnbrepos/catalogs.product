package com.freeingreturns.catalogs.product;

import static java.text.MessageFormat.format;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.Ignore;
import org.junit.Test;

import com.freeingreturns.catalogs.product.ProductCatalogMain;
import com.freeingreturns.catalogs.product.driver.DbSeeder;
import com.neeve.trace.Tracer;
import com.neeve.trace.Tracer.Level;

/**
 * tests the catalog-seeder (from database)
 */
@Ignore
public class ProductSeederTest extends AbstractTest {
    private static final Tracer logger = Tracer.create("service.product", Level.FINEST);

    /**
     * 
     */
    private Properties getProperties() {
        Properties env = new Properties();
        env.put("nv.ddl.profiles", "junit");
        env.put("x.apps.prd-processor.storage.clustering.enabled", "false");
        env.put("x.apps.prd-dbseeder.storage.clustering.enabled", "false");
        env.put("x.apps.prd-utils.storage.clustering.enabled", "false");
        env.put("appdataroot", "target/testbed/rdat");
        return env;
    }

    @Test
    public void testProductSeed() throws Throwable {
        // configure
        Properties env = getProperties();
        logger.log(format("{0} - Starting", getClass().getSimpleName()), Level.INFO);

        // start apps
        startApp(ProductCatalogMain.class, "prd-processor", "prd-instA", env);

        // start the send driver
        DbSeeder seedTest = startApp(DbSeeder.class, "prd-dbseeder", "prd-utils", env);
        seedTest.doSeed();
        seedTest.getLatch().await(100000, TimeUnit.MILLISECONDS);

        if (logger.isEnabled(Level.DIAGNOSE))
            logger.log(format("Latch count:{0}", seedTest.getLatch().getCount()), Level.DIAGNOSE);
        logger.log(format("{0} - Ending", getClass().getSimpleName()), Level.INFO);
    }

}
