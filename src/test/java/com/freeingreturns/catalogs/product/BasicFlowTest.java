package com.freeingreturns.catalogs.product;

import static java.text.MessageFormat.format;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

import com.freeingreturns.catalogs.product.ProductCatalogMain;
import com.freeingreturns.catalogs.product.driver.BasicFlow;
import com.neeve.trace.Tracer;
import com.neeve.trace.Tracer.Level;

/**
 * A test case that tests the application flow. 
 */
public class BasicFlowTest extends AbstractTest {
    private static final Tracer logger = Tracer.create("service.product", Level.FINEST);

    /**
     * 
     */
    private Properties getProperties() {
        Properties env = new Properties();
        env.put("nv.ddl.profiles", "lcldev");
        env.put("x.apps.prd-processor.storage.clustering.enabled", "false");
        env.put("x.apps.prd-dbseeder.storage.clustering.enabled", "false");
        env.put("x.apps.prd-utils.storage.clustering.enabled", "false");
        env.put("appdataroot", "target/testbed/rdat");
        return env;
    }

    @Test
    public void testFlow() throws Throwable {
        // configure
        Properties env = getProperties();

        // start apps
        //startApp(ProductCatalogMain.class, "prd-processor", "prd-instA", env);

        // start the send driver
        BasicFlow basicFlowTest = startApp(BasicFlow.class, "prd-basicflow", "prd-utils", env);
        basicFlowTest.doSend(2, 100);
        basicFlowTest.getLatch().await(10000, TimeUnit.MILLISECONDS);

        logger.log(format("Latch count:{0}", basicFlowTest.getLatch().getCount()), Level.WARNING);
    }

}
